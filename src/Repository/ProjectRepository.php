<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @extends ServiceEntityRepository<Project>
 *
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Project::class);
    }

    public function add(Project $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Project $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getCompletionDataQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('Project')
            ->select('
                Project
            ');
    }

    public function findByForm(array $filters, array $ordering, int $limit, int $offset)
    {
        $items = $this->getCompletionDataQuery();
        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == 'searchName' & !is_null($value)) {
                    $items = $items->andWhere($items->expr()->like('Project.name', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
//                    $items = $items->orWhere($items->expr()->like('Project.lastName', ':searchName'))
//                        ->setParameter('searchName', '%'.$value.'%');
//                    $items = $items->orWhere($items->expr()->like('Project.email', ':searchName'))
//                        ->setParameter('searchName', '%'.$value.'%');
                }
            }
        }
        if (!is_null($ordering)) {
            foreach ($ordering as $key => $value) {
                $items = $items->orderBy('Project.' . $key, $value);
            }
        } else {
            $items = $items->orderBy('Project.id', "ASC");
        }
        $items = $items->setMaxResults($limit);
        $items = $items->setFirstResult($offset*$limit);
        return $items->getQuery()->getResult();
    }

    public function countBy(array $filters)
    {
        $items = $this->getCompletionDataQuery();
        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == 'searchName' & !is_null($value)) {
                    $items = $items->andWhere($items->expr()->like('Project.name', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
//                    $items = $items->orWhere($items->expr()->like('Project.lastName', ':searchName'))
//                        ->setParameter('searchName', '%'.$value.'%');
//                    $items = $items->orWhere($items->expr()->like('Project.email', ':searchName'))
//                        ->setParameter('searchName', '%'.$value.'%');
                }
            }
        }
        return count($items->getQuery()->getResult());
    }
}
