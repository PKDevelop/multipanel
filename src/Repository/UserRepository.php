<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * @extends ServiceEntityRepository<User>
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function add(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(User $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);

        $this->add($user, true);
    }

    public function getCompletionDataQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('User')
            ->select('
                User
            ');
    }

    public function findByForm(array $filters, array $ordering, int $limit, int $offset)
    {
        $items = $this->getCompletionDataQuery();
        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == 'searchName' & !is_null($value)) {
                    $items = $items->andWhere($items->expr()->like('User.firstName', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
                    $items = $items->orWhere($items->expr()->like('User.lastName', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
                    $items = $items->orWhere($items->expr()->like('User.email', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
                }
                if ($key == 'email' & !is_null($value)) {
                    $items = $items->andWhere($items->expr()->eq('User.email', ':email'))
                        ->setParameter('email', $value);
                }
            }
        }
        if (!is_null($ordering)) {
            foreach ($ordering as $key => $value) {
                $items = $items->orderBy('User.' . $key, $value);
            }
        } else {
            $items = $items->orderBy('User.id', "ASC");
        }
        $items = $items->setMaxResults($limit);
        $items = $items->setFirstResult($offset*$limit);
        return $items->getQuery()->getResult();
    }

    public function countBy(array $filters)
    {
        $items = $this->getCompletionDataQuery();
        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == 'searchName' & !is_null($value)) {
                    $items = $items->andWhere($items->expr()->like('User.firstName', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
                    $items = $items->orWhere($items->expr()->like('User.lastName', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
                    $items = $items->orWhere($items->expr()->like('User.email', ':searchName'))
                        ->setParameter('searchName', '%'.$value.'%');
                }
            }
        }
        return count($items->getQuery()->getResult());
    }
}
