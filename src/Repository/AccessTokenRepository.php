<?php

namespace App\Repository;

use App\Entity\AccessToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

/**
 * @extends ServiceEntityRepository<AccessToken>
 *
 * @method AccessToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccessToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccessToken[]    findAll()
 * @method AccessToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccessTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccessToken::class);
    }

    public function getCompletionDataQuery(): QueryBuilder
    {
        return $this->createQueryBuilder('AccessToken')
            ->select('
                AccessToken
            ');
    }

    public function add(AccessToken $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(AccessToken $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByForm($filters, $ordering)
    {
        $items = $this->getCompletionDataQuery();
        if (!is_null($filters)) {
            foreach ($filters as $key => $value) {
                if ($key == 'user' & !is_null($value)) {
                    $items = $items->leftJoin('AccessToken.user', 'User');
                    $items = $items->andWhere('User.id = :id')
                        ->setParameter('id', $value);
                }
                if ($key == 'token' & !is_null($value)) {
                    $items = $items->andWhere('AccessToken.token = :token')
                        ->setParameter('token', $value);
                }
                if ($key == 'later' & !is_null($value)) {
                    $items = $items->andWhere('AccessToken.expireDate < :date')
                        ->setParameter('date', $value);
                }
            }
        }
        if (!is_null($ordering)) {
            foreach ($ordering as $key => $value) {
                $items = $items->orderBy('AccessToken.' . $key, $value);
            }
        } else {
            $items = $items->orderBy('AccessToken.id', "ASC");
        }
        return $items->getQuery()->getResult();
    }

}