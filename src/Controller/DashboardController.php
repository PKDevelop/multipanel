<?php

namespace App\Controller;

use App\Management\AccessToken;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Management\User;
use App\Management\Project;
use App\Helper\User as UserHelper;
use App\Management\EmailQueue as EmailQueueManagement;


class DashboardController extends AbstractController
{
    public function __construct(
        User $userManagement,
        Project $projectsManagement,
        UserHelper $userHelper,
        AccessToken $accessTokenManagement
    ) {
        $this->users = $userManagement;
        $this->projects = $projectsManagement;
        $this->accessTokens = $accessTokenManagement;
        $this->userHelper = $userHelper;
        $this->response = array(
            'status' => JsonResponse::HTTP_UNAUTHORIZED,
            'message' => 'Sesja wygasła'
        );
        $this->filters = array();
        $this->sorting = array();
        $this->limit = 10;
        $this->offset = 0;
        $this->page = 1;
    }

    public function index(
        Request $request,
        EmailQueueManagement $emailQueueManagement
        ): JsonResponse
    {
        $emailQueueManagement->send();
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(!empty($this->accessTokens->check($token))) {
            unset($this->response['message']);
            $this->response['users'] = $this->users->count( $this->filters);
            $this->response['projects'] = $this->projects->count( $this->filters);
            $this->response['status'] = JsonResponse::HTTP_OK;
        }
        return new JsonResponse($this->response, $this->response['status']);
    }





}
