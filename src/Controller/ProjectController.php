<?php

namespace App\Controller;

use App\Management\AccessToken;
use Cassandra\Date;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Management\Project;
use App\Management\User;
use App\Helper\Project as ProjectHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Project as ProjectEntity;


class ProjectController extends AbstractController
{
    public function __construct(
        Project $projectManagement,
        User $userManagement,
        ProjectHelper $projectHelper,
        AccessToken $accessTokenManagement,
        ManagerRegistry $doctrine
    ) {
        $this->project = $projectManagement;
        $this->user = $userManagement;
        $this->accessTokens = $accessTokenManagement;
        $this->entityManager =  $doctrine->getManager();
        $this->projectHelper = $projectHelper;
        $this->response = array(
            'status' => JsonResponse::HTTP_UNAUTHORIZED,
            'message' => 'Sesja wygasła'
        );
        $this->filters = array();
        $this->sorting = array();
        $this->limit = 10;
        $this->offset = 0;
        $this->page = 1;
    }

    public function list(
        Request $request
        ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(isset($requestContent['searchName'])){
            $this->filters['searchName'] = $requestContent['searchName'];
        }
        if(isset($requestContent['limit'])){
            $this->limit = $requestContent['limit'];
        }
        if(isset($requestContent['page'])){
            if($requestContent['page'] > 1){
                $requestContent['page']--;
            }else{
                $requestContent['page'] = 0;
            }
            $this->offset = $requestContent['page'];
        }
        if(!empty($this->accessTokens->check($token))){
            $items = $this->project->load(null, $this->filters, $this->sorting, $this->limit, $this->offset);
            $items = $this->projectHelper->getListing($items);
            $this->response['status'] = JsonResponse::HTTP_OK;
            unset($this->response['message']);
            $this->response['collection'] = $items;
            $this->response['pagination']['sumAll'] = $this->project->count( $this->filters);
            $this->response['pagination']['activePage'] = $this->page;
            $this->response['pagination']['limit'] = $this->limit;
            $this->response['pagination']['lastPage'] = ceil($this->project->count( $this->filters) / $this->limit);
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function view(
        Request $request,
        $id
        ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        if(!empty($this->accessTokens->check($token))) {
            $this->response['project'] = $this->projectHelper->loadDetails($this->project->load($id, $this->filters, $this->sorting, $this->limit, $this->offset));
            $this->response['status'] = JsonResponse::HTTP_OK;
            unset($this->response['message']);
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function update(
        Request $request,
        SerializerInterface $serializer
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(!empty($this->accessTokens->check($token))){
            $item = $this->project->load($requestContent['id'], $this->filters, $this->sorting, $this->limit, $this->offset);
            $item->setName($requestContent['name']);
            if(isset($requestContent['client'])){
                $item->setClient($this->user->load($requestContent['client'], $this->filters, $this->sorting, $this->limit, $this->offset));
            }
            $item->setStart(new \DateTime($requestContent['start']));
            $item->setDeadline(new \DateTime($requestContent['deadline']));
            $item->setStatus($requestContent['status']);
            $item->setLabels($requestContent['labels']);
            $item->setPrice($requestContent['price']);
            $item->setDescription($requestContent['description']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();
            $this->response['status'] = JsonResponse::HTTP_OK;
            $this->response['message'] = "Zapisano zmiany";
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function create(
        Request $request
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(!empty($this->accessTokens->check($token))){
            if(!empty($this->project->load(null, array('searchName' => $requestContent['name']), $this->sorting, $this->limit, $this->offset))){
                $this->response['status'] = JsonResponse::HTTP_BAD_REQUEST;
                $this->response['message'] = "Projekt o takiej nazwie już istnieje";
            }else {
                $item = new ProjectEntity();
                $item->setName($requestContent['name']);
                if(isset($requestContent['client'])){
                    $item->setClient($this->user->load($requestContent['client'], $this->filters, $this->sorting, $this->limit, $this->offset));
                }
                if($requestContent['deadline'] == ""){
                    $today = new \DateTime();
                    $item->setDeadline($today->modify('+14 days'));
                }else{
                    $item->setDeadline(new \DateTime($requestContent['deadline']));
                }
//
                $item->setStart(new \DateTime($requestContent['start']));

                $item->setStatus($requestContent['status']);
                $item->setLabels($requestContent['labels']);
                $item->setCreated(new \DateTime());
                $item->setPrice($requestContent['price']);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($item);
                $entityManager->flush();
                $this->response['status'] = JsonResponse::HTTP_OK;
                $this->response['message'] = "Dodano do bazy";
            }
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function delete(
        Request $request,
        $id
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        if(!empty($this->accessTokens->check($token))){
            try {
                $item = $this->entityManager->getRepository(ProjectEntity::class)->find($id);
                $this->entityManager->remove($item);
                $this->entityManager->flush();
            } catch (Exception $e) {
                $this->response['status'] = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
                $this->response['message'] = $e->getMessage();
            }
            $this->response['status'] = JsonResponse::HTTP_OK;
            $this->response['message'] = "Pomyślnie skasowano.";

        }
        return new JsonResponse($this->response, $this->response['status']);
    }

}
