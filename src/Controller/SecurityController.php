<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Management\AccessToken;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Helper\General;

class SecurityController extends AbstractController
{
    public function __construct(
        UserRepository $users,
        AccessToken $accessTokenManagement,
        ManagerRegistry $doctrine
    ) {
        $this->users = $users;
        $this->accessTokens = $accessTokenManagement;
        $this->entityManager =  $doctrine->getManager();
        $this->response = array(
            'status' => JsonResponse::HTTP_UNAUTHORIZED,
            'message' => 'Błędny email lub hasło.'
        );
    }

    public function login(
        Request $request, 
        UserPasswordHasherInterface $passwordHasher,
        ManagerRegistry $doctrine
        ): JsonResponse
    {
        $entityManager =  $doctrine->getManager();
        $requestContent = json_decode($request->getContent(), true);
        $user = $this->users->findOneBy(['email' => $requestContent['email']]);
        if(!is_null($user)){
            if ($passwordHasher->isPasswordValid($user, $requestContent['password'])) {
                $token = $this->accessTokens->load(null, array('user' => $user->getId()), array());
                if(!empty($token[0])){
                    $this->response['status'] = JsonResponse::HTTP_OK;
                    $this->response['message'] = 'Pomyślnie zalogowano';
                    $this->response['user'] = array(
                        'id' => $user->getId(),
                        'email' => $user->getEmail(),
                        'firstName' => $user->getFirstName(),
                        'lastName' => $user->getLastName(),
                        'exp' => General::dateTime($token[0]->getExpireDate())
                    );
                    $this->response['user']['token'] = $this->accessTokens->load(null, array('user' => $user->getId()), array())[0]->getToken();
                }else{
                    $token = bin2hex(random_bytes(22));
                    try {
                        $entityManager->persist($this->accessTokens->create($user, $token));
                        $entityManager->flush();
                        $this->response['status'] = JsonResponse::HTTP_OK;
                        $this->response['message'] = 'Ponownie zalogowano';
                        $this->response['user'] = array(
                            'id' => $user->getId(),
                            'email' => $user->getEmail(),
                            'firstName' => $user->getFirstName(),
                            'lastName' => $user->getLastName(),
                            'token' => $token,
                            'exp' => General::dateTime($this->accessTokens->load(null, array('token'=>$token), array())[0]->getExpireDate())
                        );
                    } catch (Exception $e) {
                        $this->response['status'] = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
                        $this->response['message'] = $e->getMessage();
                    }
                }
            }
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function checkToken(
        Request $request
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $checkStatus = $this->accessTokens->check($token);
        if(empty($checkStatus)){
            $this->response['message'] = 'Sesja wygasła';
        }else{
            $this->response['status'] = JsonResponse::HTTP_OK;
            $this->response['message'] = 'Przedłużono token';
            $this->response['exp'] = General::dateTime($checkStatus->getExpireDate());
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function removeExpiredTokens(){
        $tokens = $this->accessTokens->load(null, array('later' => new \DateTime()), array());
        foreach($tokens as $token){
            $this->accessTokens->remove($token->getId());
        }
        return new JsonResponse('Clear', JsonResponse::HTTP_OK);
    }
}
