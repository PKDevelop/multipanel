<?php

namespace App\Controller;

use App\Management\AccessToken;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Management\User;
use App\Helper\User as UserHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\User as UserEntity;


class UserController extends AbstractController
{
    public function __construct(
        User $userManagement,
        UserHelper $userHelper,
        AccessToken $accessTokenManagement,
        ManagerRegistry $doctrine
    ) {
        $this->users = $userManagement;
        $this->accessTokens = $accessTokenManagement;
        $this->entityManager =  $doctrine->getManager();
        $this->userHelper = $userHelper;
        $this->response = array(
            'status' => JsonResponse::HTTP_UNAUTHORIZED,
            'message' => 'Sesja wygasła'
        );
        $this->filters = array();
        $this->sorting = array();
        $this->limit = 10;
        $this->offset = 0;
        $this->page = 1;
    }

    public function list(
        Request $request
        ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(isset($requestContent['searchName'])){
            $this->filters['searchName'] = $requestContent['searchName'];
        }
        if(isset($requestContent['limit'])){
            $this->limit = $requestContent['limit'];
        }
        if(isset($requestContent['page'])){
            if($requestContent['page'] > 1){
                $requestContent['page']--;
            }else{
                $requestContent['page'] = 0;
            }
            $this->offset = $requestContent['page'];
        }
        if(!empty($this->accessTokens->check($token))){
            $items = $this->users->load(null, $this->filters, $this->sorting, $this->limit, $this->offset);
            $items = $this->userHelper->getListing($items);
            $this->response['status'] = JsonResponse::HTTP_OK;
            unset($this->response['message']);
            $this->response['collection'] = $items;
            $this->response['pagination']['sumAll'] = $this->users->count( $this->filters);
            $this->response['pagination']['activePage'] = $this->page;
            $this->response['pagination']['limit'] = $this->limit;
            $this->response['pagination']['lastPage'] = ceil($this->users->count( $this->filters) / $this->limit);
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function view(
        Request $request,
        $id
        ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        if(!empty($this->accessTokens->check($token))) {
            $this->response['user'] = $this->userHelper->loadDetails($this->users->load($id, $this->filters, $this->sorting, $this->limit, $this->offset));
            $this->response['status'] = JsonResponse::HTTP_OK;
            unset($this->response['message']);
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function update(
        Request $request,
        SerializerInterface $serializer
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(!empty($this->accessTokens->check($token))){
            $item = $this->users->load($requestContent['id'], $this->filters, $this->sorting, $this->limit, $this->offset);
            $item->setFirstName($requestContent['firstName']);
            $item->setLastName($requestContent['lastName']);
            $item->setAddressLine($requestContent['addressLine']);
            $item->setAddressCity($requestContent['addressCity']);
            $item->setAddressPostCode($requestContent['addressPostCode']);
            $item->setAddressCountry($requestContent['addressCountry']);
            $item->setPhone($requestContent['phone']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();
            $this->response['status'] = JsonResponse::HTTP_OK;
            $this->response['message'] = "Zapisano zmiany";

        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function create(
        Request $request,
        SerializerInterface $serializer
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        $requestContent = json_decode($request->getContent(), true);
        if(!empty($this->accessTokens->check($token))){
            if(!empty($this->users->load(null, array('email' => $requestContent['email']), $this->sorting, $this->limit, $this->offset))){
                $this->response['status'] = JsonResponse::HTTP_BAD_REQUEST;
                $this->response['message'] = "E-mail istnieje";
            }else {
                $item = new UserEntity();
                $item->setFirstName($requestContent['firstName']);
                $item->setEmail($requestContent['email']);
                $item->setLastName($requestContent['lastName']);
                $item->setAddressLine($requestContent['addressLine']);
                $item->setAddressCity($requestContent['addressCity']);
                $item->setAddressPostCode($requestContent['addressPostCode']);
                $item->setAddressCountry($requestContent['addressCountry']);
                $item->setPassword('$2y$13$NjmT9aZSaN58LcHSlWW7KOdeAlBHB5.9ZfiUxP20yNLjJZZQhzla2');
                $item->setRoles(array("ROLE_USER"));
                $item->setPhone($requestContent['phone']);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($item);
                $entityManager->flush();
                $this->response['status'] = JsonResponse::HTTP_CREATED;
                $this->response['message'] = "Dodano do bazy";
            }
        }
        return new JsonResponse($this->response, $this->response['status']);
    }

    public function delete(
        Request $request,
        $id
    ): JsonResponse
    {
        $token = $request->headers->get('X-AUTH-TOKEN') ?? '';
        if(!empty($this->accessTokens->check($token))){
            try {
                $item = $this->entityManager->getRepository(UserEntity::class)->find($id);
                $this->entityManager->remove($item);
                $this->entityManager->flush();
            } catch (Exception $e) {
                $this->response['status'] = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
                $this->response['message'] = $e->getMessage();
            }
            $this->response['status'] = JsonResponse::HTTP_OK;
            $this->response['message'] = "Pomyślnie skasowano.";

        }
        return new JsonResponse($this->response, $this->response['status']);
    }

}
