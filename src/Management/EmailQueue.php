<?php

namespace App\Management;

use App\Entity\EmailQueue as Entity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailQueue
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailerInterface $mailerInterface
    ) {
        $this->entityManager = $entityManager;
        $this->mailerInterface = $mailerInterface;
    }

    public function load($id, $filters, $ordering)
    {
        if (is_null($id)) {
            $items = $this->entityManager->getRepository(Entity::class)->findByForm($filters, $ordering);
        } else {
            $items = $this->entityManager->getRepository(Entity::class)->find($id);
        }
        if (empty($items)) {
            return array();
        }
        return $items;
    }

    public function create($from, $to, $subject, $template)
    {
        $item = new Entity();
        $item->setFromAddress($from);
        $item->setToAddress($to);
        $item->setSubject($subject);
        $item->setTemplate($template);
        $item->setInsertDate(new \DateTime());
        $item->setSend(0);
        $this->entityManager->persist($item);
        $this->entityManager->flush();
        $this->entityManager->clear();
        return $item;
    }

    public function send($id = null){
        $emailsQueue = $this->entityManager->getRepository(Entity::class)->findBy(array("send" => 0), null, 1);
        foreach ($emailsQueue as $item){
            $email = (new Email())
            ->from($item->getFromAddress())
            ->to($item->getToAddress())
            ->replyTo($item->getFromAddress())
            ->priority(Email::PRIORITY_HIGH)
            ->subject($item->getSubject())
            // ->attach($data, $invoice['number'].'.pdf', 'application/pdf')
            ->html($item->getTemplate());
            if(is_null($this->mailerInterface->send($email))){
                $this->markSended($item->getId());
            }
        }
    }

    public function markSended($id){
        $item = $this->entityManager->getRepository(Entity::class)->find($id);
        $item->setSendDate(new \DateTime());
        $item->setSend(1);
        $this->entityManager->persist($item);
        $this->entityManager->flush();
        $this->entityManager->clear();
    }

    public function remove($id)
    {
        $item = $this->entityManager->getRepository(Entity::class)->find($id);
        $this->entityManager->remove($item);
        $this->entityManager->flush();
        return true;
    }

}