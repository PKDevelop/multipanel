<?php

namespace App\Management;

use App\Entity\User as Entity;
use Doctrine\ORM\EntityManagerInterface;

class User
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function load($id, $filters, $ordering, $limit = 10, $offset = 0)
    {
        if (is_null($id)) {
            $items = $this->entityManager->getRepository(Entity::class)->findByForm($filters, $ordering, $limit, $offset);
        } else {
            $items = $this->entityManager->getRepository(Entity::class)->find($id);
        }
        if (empty($items)) {
            return array();
        }
        return $items;
    }

    public function count($filters)
    {
        return $this->entityManager->getRepository(Entity::class)->countBy($filters);
    }

//    public function create($user, $token)
//    {
//        $currentDateTime = new \DateTime();
//        $expirationDateTime = clone $currentDateTime;
//        $expirationDateTime->modify('+30 minutes');
//        $item = new Entity();
//        $item->setToken($token);
//        $item->setCreateDate($currentDateTime);
//        $item->setExpireDate($expirationDateTime);
//        $item->setUser($user);
//        return $item;
//    }



    public function remove($id)
    {
        $item = $this->entityManager->getRepository(Entity::class)->find($id);
        $this->entityManager->remove($item);
        $this->entityManager->flush();
        return true;
    }
}