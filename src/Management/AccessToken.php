<?php

namespace App\Management;

use App\Entity\AccessToken as Entity;
use Doctrine\ORM\EntityManagerInterface;

class AccessToken
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function load($id, $filters, $ordering)
    {
        if (is_null($id)) {
            $items = $this->entityManager->getRepository(Entity::class)->findByForm($filters, $ordering);
        } else {
            $items = $this->entityManager->getRepository(Entity::class)->find($id);
        }
        if (empty($items)) {
            return array();
        }
        return $items;
    }

    public function create($user, $token)
    {
        $currentDateTime = new \DateTime();
        $expirationDateTime = clone $currentDateTime;
        $expirationDateTime->modify('+30 minutes');
        $item = new Entity();
        $item->setToken($token);
        $item->setCreateDate($currentDateTime);
        $item->setExpireDate($expirationDateTime);
        $item->setUser($user);
        return $item;
    }

    public function check($token)
    {
        if(empty($this->entityManager->getRepository(Entity::class)->findByForm(array('token' => $token), array()))){
            return false;
        }else{
            $currentDateTime = new \DateTime();
            $expirationDateTime = clone $currentDateTime;
            $expirationDateTime->modify('+30 minutes');
            $item = $this->entityManager->getRepository(Entity::class)->findByForm(array('token' => $token), array())[0];
            $item->setExpireDate($expirationDateTime);
            $this->entityManager->persist($item);
            $this->entityManager->flush();
            return $item;
        }

    }

    public function remove($id)
    {
        $item = $this->entityManager->getRepository(Entity::class)->find($id);
        $this->entityManager->remove($item);
        $this->entityManager->flush();
        return true;
    }
}