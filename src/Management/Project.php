<?php

namespace App\Management;

use App\Entity\Project as Entity;
use Doctrine\ORM\EntityManagerInterface;

class Project
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function load($id, $filters, $ordering, $limit = 10, $offset = 0)
    {
        if (is_null($id)) {
            $items = $this->entityManager->getRepository(Entity::class)->findByForm($filters, $ordering, $limit, $offset);
        } else {
            $items = $this->entityManager->getRepository(Entity::class)->find($id);
        }
        if (empty($items)) {
            return array();
        }
        return $items;
    }

    public function count($filters)
    {
        return $this->entityManager->getRepository(Entity::class)->countBy($filters);
    }

    public function remove($id)
    {
        $item = $this->entityManager->getRepository(Entity::class)->find($id);
        $this->entityManager->remove($item);
        $this->entityManager->flush();
        return true;
    }
}