<?php

namespace App\Helper;

class User
{
    public function __construct(
    ) {
        $this->listing = array(
            "collectionSum" => 0,
            "items" => array()
        );
    }
    
    public function getListing($items)
    {
        $this->listing = array();
        if(!is_null($items)){
            foreach ($items as $key => $item) {
                $this->listing [] = [
                    'lp' => $key + 1,
                    'id' => $item->getId(),
                    'firstName' => $item->getFirstName(),
                    'lastName' => $item->getLastName(),
                    'email' => $item->getEmail()
                ];
            }
        }
        return $this->listing;
    }
    public function loadDetails($item)
    {
        $item = array(
            'id' => $item->getId(),
            'firstName' => $item->getFirstName(),
            'lastName' => $item->getLastName(),
            'email' => $item->getEmail(),
            'phone' => $item->getPhone(),
            'position' => $item->getPosition(),
            'addressLine' => $item->getAddressLine(),
            'addressCity' => $item->getAddressCity(),
            'addressPostCode' => $item->getAddressPostCode(),
            'addressCountry' => $item->getAddressCountry(),
        );

        return $item;
    }


}
