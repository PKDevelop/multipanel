<?php

namespace App\Helper;

use App\Helper\General;

class Project
{
    public function __construct(
    ) {
        $this->listing = array(
            "collectionSum" => 0,
            "items" => array()
        );
    }
    
    public function getListing($items)
    {
        $this->listing = array();
        if(!is_null($items)){
            foreach ($items as $key => $item) {
                $client = null;
                if($item->getClient()){
                    $client = array(
                        'id' => $item->getClient()->getId(),
                        'name' => $item->getClient()->getFirstName().' '.$item->getClient()->getLastName()
                    );
                }
                $this->listing [] = [
                    'lp' => $key + 1,
                    'id' => $item->getId(),
                    'name' => $item->getName(),
                    'client' => $client,
                    'deadline' => General::date($item->getDeadline()),
                    'status' => General::projectStatus($item->getStatus()),
                    'labels' => $item->getLabels(),
                    'price' => General::price($item->getPrice())
                ];

            }
        }
        return $this->listing;
    }
    public function loadDetails($item)
    {
        $item = array(
            'id' => $item->getId(),
            'name' => $item->getName(),
            'start' => General::date($item->getStart()),
            'deadline' => General::date($item->getDeadline()),
            'status' => $item->getStatus(),
//            'status' => General::projectStatus($item->getStatus()),
            'labels' => $item->getLabels(),
            'price' => $item->getPrice(),
//            'price' => General::price($item->getPrice()),
            'description' => $item->getDescription()
        );
        return $item;
    }


}
