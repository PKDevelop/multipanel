<?php

namespace App\Helper;

class General
{
    public function __construct(
    ) {}


    public static function price($price){
        return number_format($price, 2).' PLN';
    }
    public static function date($date){
        return date_format($date, 'd-m-Y');
    }

    public static function dateTime($date){
        return date_format($date, 'd-m-Y H:i:s');
    }

    public static function projectStatus($status){
        $projectStatus = array(
            'class' => 'bg-lightblue',
            'label'=> 'Nowy projekt'
        );
        $projectStatus['id'] = $status;
        switch ($status) {
            case 2:
                $projectStatus['class'] = 'bg-purple';
                $projectStatus['label'] = 'W realizacji';
                break;
            case 3:
                $projectStatus['class'] = 'bg-maroon';
                $projectStatus['label'] = 'Wstrzymany';
                break;
            case 4:
                $projectStatus['class'] = 'bg-warning';
                $projectStatus['label'] = 'Anulowany';
                break;
            case 5:
                $projectStatus['class'] = 'bg-olive';
                $projectStatus['label'] = 'Zakonczony';
                break;
        }
        return $projectStatus;
    }

}
