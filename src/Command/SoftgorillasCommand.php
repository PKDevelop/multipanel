<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle as SymfonyStyleAlias;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class SoftgorillasCommand extends Command
{
    protected static $defaultName = 'app:add-softgorillas';
    protected static $defaultDescription = 'Separating reports from a Json file into two other files.';
    private SymfonyStyleAlias $io;

    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp($this->getCommandHelp())
            ->addArgument('file', InputArgument::OPTIONAL, 'The name of the file (without an extension) in the public folder.')
        ;
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyleAlias($input, $output);
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        if (null !== $input->getArgument('file')) {
            return;
        }

        $this->io->title('Separating reports from a Json file into two other files.');
        $this->io->text([
            'The JSON file contains data that will be filtered and divided into two other files.',
            'The system search only works on JSON files, so you do not need to provide the file extension.',
            'Please type a neme of the file.',
        ]);

        /** @var string|null $file */
        $file = $input->getArgument('file');

        if (null == $file) {
            $file = $this->io->ask('File name (without an extension)', null);
            $input->setArgument('file', $file);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string $file */
        $file = $input->getArgument('file');
        $stopwatch = new Stopwatch();
        $stopwatch->start('add-softgorillas');
        $crashes = [];
        $reviews = [];
        $descriptions = [];
        $crashesCount = 0;
        $reviewsCount = 0;
        $directory = $this->container->getParameter('kernel.project_dir');
        $filePath = $directory . '/public/' . $file.'.json';

        if (file_exists($filePath)) {
            $jsonContent = file_get_contents($filePath);
            $jsonData = json_decode($jsonContent, true);
            foreach($jsonData as $case){
                if (!in_array($case['description'], $descriptions)) {
                    if (strpos($case['description'], 'przegląd') !== false) {
                        $reviews [] = array(
                            'description' => $case['description'],
                            'type' => 'przegląd',
                            'date' => $case['dueDate'] ? date('Y-m-d', strtotime($case['dueDate'])) : null,
                            'week_of_the_year' => $case['dueDate'] ? date('W', strtotime($case['dueDate'])) : null,
                            'status' => $case['dueDate'] ? 'zaplanowano' : 'nowy',
                            'next term' => null,
                            'contact_phone' => $case['phone'],
                            'created' => null,
                        );
                        $reviewsCount++;
                    } else {
                        $crashes [] = array(
                            'description' => $case['description'],
                            'type' => 'zgłoszenie awarii',
                            'priority' => $this->setPriority($case['description']),
                            'date' => $case['dueDate'] ? date('Y-m-d', strtotime($case['dueDate'])) : null,
                            'status' => $case['dueDate'] ? 'termin' : 'nowy',
                            'comments' => null,
                            'contact_phone' => $case['phone'],
                            'created' => null,
                        );
                        $crashesCount++;
                    }
                    $descriptions[]=$case['description'];
                }
            }
            $reviewsData = json_encode($reviews, JSON_UNESCAPED_UNICODE);
            $crashesData = json_encode($crashes, JSON_UNESCAPED_UNICODE);
            $filePath = $directory . '/public/';
            file_put_contents($filePath.'reviews.json', $reviewsData);
            file_put_contents($filePath.'crashes.json', $crashesData);
//            file_put_contents($directory . '/public/reviews.json', json_encode(array_values($reviews), JSON_PRETTY_PRINT));
//            file_put_contents($directory . '/public/crashes.json', json_encode(array_values($crashes), JSON_PRETTY_PRINT));
            $this->io->success('Script executed successfully!');
            $this->io->info('Number of messages processed: '.count($jsonData));
            $this->io->info('Number of reviews created: '.$reviewsCount.' file with results available at: '.$directory . '/public/reviews.json');
            $this->io->info('Number of crashes reports created: '.$crashesCount.' file with results available at '.$directory . '/public/crashes.json');
        } else {
            $this->io->error('Cant find file: '.$file.'.json');
        }

        return Command::SUCCESS;
    }

    private function setPriority($description){
        $priority = 'normalny';
        if (stripos($description, 'bardzo pilne') !== false) {
            $priority = 'krytyczny';
        } elseif (stripos($description, 'pilne') !== false) {
            $priority = 'wysoki';
        }
        return $priority;
    }


    private function getCommandHelp(): string
    {
        return 'The <info>%command.name%</info> command to separation reports from a Json file into two other files. First, after starting, you must provide a file name without extension. The script looks for a .json file in the applications public folder';
    }
}