<?php

namespace App\Command;

use App\Entity\User;
use App\Management\AccessToken;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use function Symfony\Component\String\u;

final class RemoveExpireAccessTokensCommand extends Command
{

    protected static $defaultName = 'app:remove-expire-access-tokens';
    protected static $defaultDescription = 'Remove expire access tokens from the database';

    public function __construct(
        EntityManagerInterface $entityManager,
        AccessToken $accessTokenManagement
    ) {
        $this->entityManager = $entityManager;
        $this->accessTokens = $accessTokenManagement;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('remove-expire-access-tokens');

        $tokens = $this->accessTokens->load(null, array('later' => new \DateTime()), array());

        foreach($tokens as $token){
            $this->accessTokens->remove($token->getId());
        }

        return Command::SUCCESS;
    }

}
